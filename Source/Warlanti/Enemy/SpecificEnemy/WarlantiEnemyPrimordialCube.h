// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Enemy/WarlantiEnemy.h"
#include "Enemy/WarlantiEnemyHitbox.h"
#include "WarlantiEnemyPrimordialCube.generated.h"

/**
 * 
 */
UCLASS()
class WARLANTI_API AWarlantiEnemyPrimordialCube : public AWarlantiEnemy
{
	GENERATED_BODY()
	
public:
	// Sets default values for this pawn's properties
	AWarlantiEnemyPrimordialCube();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);

protected:
	UPROPERTY(Category = Warlanti, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UWarlantiEnemyHitbox* NormalHitbox;

	UPROPERTY(Category = Warlanti, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UWarlantiEnemyHitbox* VulnarableHitbox;
};

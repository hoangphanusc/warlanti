// Fill out your copyright notice in the Description page of Project Settings.

#include "WarlantiEnemyPrimordialCube.h"
#include "Enemy/WarlantiEnemyMovementComponent.h"
#include "Attack/AttackInstanceActor.h"
#include "Engine.h"

// Sets default values
AWarlantiEnemyPrimordialCube::AWarlantiEnemyPrimordialCube() : AWarlantiEnemy()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create an instance of our movement component, and tell it to update the root.
	NormalHitbox = CreateDefaultSubobject<UWarlantiEnemyHitbox>(TEXT("NormalHitbox"));
	NormalHitbox->SetupAttachment(RootComponent);


	VulnarableHitbox = CreateDefaultSubobject<UWarlantiEnemyHitbox>(TEXT("VulnerableHitbox"));
	VulnarableHitbox->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWarlantiEnemyPrimordialCube::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWarlantiEnemyPrimordialCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float AWarlantiEnemyPrimordialCube::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		health -= ActualDamage;
		if (health < 0) health = 0;
		healthProgress = health / maxHealth;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString("Hit!, health remaining = ") + FString::SanitizeFloat(health));
		// If the damage depletes our health set our lifespan to zero - which will destroy the actor  
		if (health <= 0.f)
		{
			SetLifeSpan(0.001f);
		}
	}

	return ActualDamage;
}


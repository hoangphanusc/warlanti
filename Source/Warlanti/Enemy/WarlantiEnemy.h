// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "WarlantiEnemy.generated.h"

UCLASS()
class WARLANTI_API AWarlantiEnemy : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AWarlantiEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Returns CharacterMovement subobject **/
	virtual UPawnMovementComponent* GetMovementComponent() const override { return (UPawnMovementComponent*) MovementComponent; };

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);

protected:
	UPROPERTY(Category = Collision, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* PhysicsBody;
	
	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UWarlantiEnemyMovementComponent* MovementComponent;


public:
	//Warlanti variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Warlanti)
	float maxHealth = 100;

	float health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Warlanti)
	float healthProgress = 1.0;
	
};
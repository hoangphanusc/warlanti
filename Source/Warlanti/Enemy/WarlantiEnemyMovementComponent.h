// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "WarlantiEnemyMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class WARLANTI_API UWarlantiEnemyMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()
public:
	void Fall(float DeltaTime);
private:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
};

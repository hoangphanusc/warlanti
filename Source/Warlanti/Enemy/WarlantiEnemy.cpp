// Fill out your copyright notice in the Description page of Project Settings.

#include "WarlantiEnemy.h"
#include "WarlantiEnemyMovementComponent.h"
#include "Engine.h"

/*	
	This is the base class for all enemies in Warlanti.
	It has a collision box for physics, movement component, and some health vars set up
*/

// Sets default values
AWarlantiEnemy::AWarlantiEnemy()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PhysicsBody = CreateDefaultSubobject<UBoxComponent>(TEXT("PhysicsBody"));
	SetRootComponent(PhysicsBody);

	// Create an instance of our movement component, and tell it to update the root.
	MovementComponent = CreateDefaultSubobject<UWarlantiEnemyMovementComponent>(TEXT("WarlantiEnemyMovementComponent"));
	MovementComponent->UpdatedComponent = RootComponent;
}

// Called when the game starts or when spawned
void AWarlantiEnemy::BeginPlay()
{
	Super::BeginPlay();
	health = maxHealth;
	healthProgress = 1.0f;
	
}

// Called every frame
void AWarlantiEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AWarlantiEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float AWarlantiEnemy::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}
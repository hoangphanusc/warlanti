// Fill out your copyright notice in the Description page of Project Settings.

#include "WarlantiEnemyMovementComponent.h"

void UWarlantiEnemyMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	Fall(DeltaTime);
};

static float fallSpeed = 0.f;
void UWarlantiEnemyMovementComponent::Fall(float DeltaTime) {
	// Make sure that everything is still valid, and that we are allowed to move.
	if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	// Get (and then clear) the movement vector that we set in AWarlantiEnemy::Tick
	fallSpeed += 10.f;
	FVector DesiredMovementThisFrame = FVector(0.f, 0.f, -1.f).GetClampedToMaxSize(1.0f) * DeltaTime * fallSpeed;
	if (!DesiredMovementThisFrame.IsNearlyZero())
	{
		FHitResult Hit;
		SafeMoveUpdatedComponent(DesiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, Hit);

		// If we bumped into something, try to slide along it
		if (Hit.IsValidBlockingHit())
		{
			fallSpeed = 0;
			SlideAlongSurface(DesiredMovementThisFrame, 1.f - Hit.Time, Hit.Normal, Hit);
		}
	}
}
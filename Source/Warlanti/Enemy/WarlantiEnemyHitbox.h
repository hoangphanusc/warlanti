// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "WarlantiEnemyHitbox.generated.h"

/**
 * 
 */
UCLASS()
class WARLANTI_API UWarlantiEnemyHitbox : public UBoxComponent
{
	GENERATED_BODY()

public:
	float GetMultDamamgeModifier() { return MultDamamgeModifier; }

	float GetFlatDamamgeModifier() { return FlatDamamgeModifier; }


private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Warlanti, meta = (AllowPrivateAccess = "true"))
	float MultDamamgeModifier = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Warlanti, meta = (AllowPrivateAccess = "true"))
	float FlatDamamgeModifier = 0.0f;
};

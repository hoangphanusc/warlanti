// Fill out your copyright notice in the Description page of Project Settings.

#include "AttackInstanceActor.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectGlobals.h"
#include "Runtime/Engine/Classes/GameFramework/ProjectileMovementComponent.h"
#include "Enemy/WarlantiEnemy.h"
#include "Enemy/WarlantiEnemyHitbox.h"
#include "Engine.h"

// Sets default values
AAttackInstanceActor::AAttackInstanceActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation
	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	CollisionComponent->InitSphereRadius(5.0f);
	CollisionComponent->BodyInstance.SetCollisionProfileName("Projectile");			// Collision profiles are defined in DefaultEngine.ini
	CollisionComponent->OnComponentHit.AddDynamic(this, &AAttackInstanceActor::OnHit);		// set up a notification for when this component overlaps something
	RootComponent = CollisionComponent;

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovement->ProjectileGravityScale = 0;
	ProjectileMovement->InitialSpeed = 200.f;
	ProjectileMovement->MaxSpeed = 10000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	
}

// Called when the game starts or when spawned
void AAttackInstanceActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AAttackInstanceActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float addSpeed = (ProjectileMovement->MaxSpeed - ProjectileMovement->InitialSpeed) / InitialLifeSpan * DeltaTime;
	FVector currVelo = ProjectileMovement->Velocity;
	currVelo.Normalize();
	ProjectileMovement->Velocity += addSpeed * currVelo;
}

///*
void AAttackInstanceActor::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		if (OtherComp->IsA(UWarlantiEnemyHitbox::StaticClass())) {
			// Create a damage event  
			TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
			FDamageEvent DamageEvent(ValidDamageTypeClass);

			float multDmgMod = ((UWarlantiEnemyHitbox*)OtherComp)->GetMultDamamgeModifier();
			float flatDmgMod = ((UWarlantiEnemyHitbox*)OtherComp)->GetFlatDamamgeModifier();
			GEngine->AddOnScreenDebugMessage(-1, 8.0f, FColor::Green, FString("Projectile hit for ") + FString::SanitizeFloat(Attack2Damage * multDmgMod + flatDmgMod));
			OtherActor->TakeDamage(Attack2Damage * multDmgMod + flatDmgMod, DamageEvent, nullptr, this);
		}

		Destroy();
	}
}
//*/
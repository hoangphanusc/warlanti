// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "WarlantiCharacter.h"
#include "Attack/AttackInstanceActor.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Enemy/WarlantiEnemy.h"
#include "Engine.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Particles/ParticleSystem.h"

#include <windows.h>
#include <string>

//////////////////////////////////////////////////////////////////////////
// AWarlantiCharacter

void AWarlantiCharacter::BeginPlay() {
	Super::BeginPlay();
	CameraBoom->TargetArmLength = DefaultCameraTrailingDistance;
	togglingFlight = false;
}

AWarlantiCharacter::AWarlantiCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = DefaultCameraTrailingDistance; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AWarlantiCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AWarlantiCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AWarlantiCharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AWarlantiCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AWarlantiCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &AWarlantiCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("TurnRate", this, &AWarlantiCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &AWarlantiCharacter::LookUpAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AWarlantiCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AWarlantiCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AWarlantiCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AWarlantiCharacter::OnResetVR);

	//Binding Warlanti handlers
	PlayerInputComponent->BindAction("Attack1", IE_Pressed, this, &AWarlantiCharacter::Attack1);
	PlayerInputComponent->BindAction("Attack2", IE_Pressed, this, &AWarlantiCharacter::Attack2);

	PlayerInputComponent->BindAction("ToggleFlight", IE_Pressed, this, &AWarlantiCharacter::OnFlightToggle);
}


void AWarlantiCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AWarlantiCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	//if the player is toggling flight, disregard camera and movement input
	if (togglingFlight) return;
	Jump();
}

void AWarlantiCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	//if the player is toggling flight, disregard camera and movement input
	if (togglingFlight) return;
	StopJumping();
}
void AWarlantiCharacter::TurnAtRate(float Rate)
{
	//if the player is toggling flight, disregard camera and movement input
	if (togglingFlight) return;

	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AWarlantiCharacter::LookUpAtRate(float Rate)
{
	//if the player is toggling flight, disregard camera and movement input
	if (togglingFlight) return;

	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AWarlantiCharacter::MoveForward(float Value)
{
	//if the player is toggling flight, disregard camera and movement input
	if (togglingFlight) return;

	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		FVector Direction;
		if (walking) {
			Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		}
		else {
			Direction = FRotationMatrix(Rotation).GetUnitAxis(EAxis::X);
		}
		AddMovementInput(Direction, Value);
	}
}

void AWarlantiCharacter::MoveRight(float Value)
{
	//if the player is toggling flight, disregard camera and movement input
	if (togglingFlight) return;

	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AWarlantiCharacter::Jump() {
	pressedJump = true;
	if (walking) {
		FTimerHandle JumpHandle;
		GetWorldTimerManager().SetTimer(JumpHandle, this, &AWarlantiCharacter::DelegateJump, .5f, false);
	}
	else {
		DelegateJump();
	}
}

void AWarlantiCharacter::DelegateJump() {
	pressedJump = false;
	Super::Jump();
}

void AWarlantiCharacter::StopJumping() {
	if (walking) {
		FTimerHandle StopJumpHandle;
		GetWorldTimerManager().SetTimer(StopJumpHandle, this, &AWarlantiCharacter::DelegateStopJumping, .5f, false);
	}
	else {
		DelegateStopJumping();
	}
}

void AWarlantiCharacter::DelegateStopJumping() {
	Super::StopJumping();
}

void AWarlantiCharacter::Attack1()
{
	if (walking) {
		//Target the attack by camera position and direction
		FVector CamLoc;
		FRotator CamRot;
		Controller->GetPlayerViewPoint(CamLoc, CamRot); // Get the camera position and rotation
		const FVector StartTrace = CamLoc; // trace start is the camera location
		FVector Direction = CamRot.Vector();
		const FVector EndTrace = StartTrace + Direction * 10000.0f;

		//See where the attack hit (ground/enemy)
		FCollisionQueryParams TraceParams(FName(TEXT("Attack1Trace")), true, this);
		TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = true;
		TraceParams.TraceTag = "Attack1DebugTrace";

		GetWorld()->DebugDrawTraceTag = "Attack1DebugTrace";

		FHitResult initialHit(ForceInit);
		GetWorld()->LineTraceSingleByChannel(initialHit, StartTrace, EndTrace, ECC_WorldDynamic, TraceParams); // simple trace function

		//If attack hit something in range, find enemies in attack radius around impact point
		if (initialHit.GetActor() != nullptr && (initialHit.ImpactPoint - GetActorLocation()).Size() <= Attack1Range) {
			//Play effect
			UParticleSystemComponent* particle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Attack1Effect, initialHit.Location, FRotator());
			particle->SetWorldScale3D(FVector(5.f, 5.f, 5.f));

			FVector impactLoc = initialHit.ImpactPoint;
			TArray<AActor*> foundEnemies;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWarlantiEnemy::StaticClass(), foundEnemies);
			for (auto& actor : foundEnemies)
			{
				// Create a damage event  
				TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
				FDamageEvent DamageEvent(ValidDamageTypeClass);

				//If it was a direct hit, take full damage
				if (actor == initialHit.GetActor()) {
					GEngine->AddOnScreenDebugMessage(-1, 8.0f, FColor::Green, FString("Direct hit"));
					actor->TakeDamage(Attack1Damage, DamageEvent, Controller, this);
				}

				//Else calculate the distance from impact point to the enemy then apply damage with falloff
				else {
					GEngine->AddOnScreenDebugMessage(-1, 8.0f, FColor::Green, FString("Damage fell off"));
					FVector actorLoc = actor->GetActorLocation();

					//Set up 2ndary hit to determine the closest part of enemies to the impact point
					GetWorld()->DebugDrawTraceTag = "Attack1DebugTrace";

					FHitResult secondaryHit(ForceInit);
					GetWorld()->LineTraceSingleByChannel(secondaryHit, impactLoc, actorLoc, ECC_WorldDynamic, TraceParams); // simple trace function

					FVector impactLoc2 = secondaryHit.ImpactPoint;
					float distance = (impactLoc - impactLoc2).Size();
					if (distance <= Attack1AOE) {
						float falloff = 1 - (distance / Attack1AOE);
						actor->TakeDamage(Attack1Damage * falloff, DamageEvent, Controller, this);
					}
				}
			}
		}
	}
	else { //flying
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;

		FRotator camRot = Controller->GetControlRotation();
		FVector camLoc = CameraBoom->GetComponentLocation();
		FVector myLoc = camLoc + camRot.Vector() * 100.f;

		GetWorld()->SpawnActor(Attack2Projectile, &myLoc, &camRot, spawnParams);
	}
}

void AWarlantiCharacter::Attack2()
{
	
}

void AWarlantiCharacter::OnFlightToggle() {
	togglingFlight = true;

	startYaw = Controller->GetControlRotation().Yaw;
	startPitch = Controller->GetControlRotation().Pitch;
	targetYaw = GetActorRotation().Yaw < 0 ? GetActorRotation().Yaw + 360.0f : GetActorRotation().Yaw;
	if (!FlightToggleTimerHandle.IsValid()) {
		GetWorldTimerManager().SetTimer(FlightToggleTimerHandle, this, &AWarlantiCharacter::ToggleFlight, FlightToggleTickRate, true);
	}
	else {
		GetWorldTimerManager().ClearTimer(FlightToggleTimerHandle);
		walking = !walking;
		GetWorldTimerManager().SetTimer(FlightToggleTimerHandle, this, &AWarlantiCharacter::ToggleFlight, FlightToggleTickRate, true);
	}
}

void AWarlantiCharacter::ToggleFlight() {
	float numTicks = FlightToggleDelay / FlightToggleTickRate;

	//Walking to flying
	if (walking) {
		//Reduce camera distance
		float armLengthDiff = DefaultCameraTrailingDistance - InFlightCameraTrailingDistance;
		CameraBoom->TargetArmLength -= armLengthDiff / numTicks;

		//Rotate camera to behind player
		if (targetYaw - startYaw > -180.f) {
			AddControllerYawInput((targetYaw - startYaw) / ((APlayerController*)Controller)->InputYawScale / numTicks);
		}
		else {
			AddControllerYawInput((targetYaw - startYaw + 360) / ((APlayerController*)Controller)->InputYawScale / numTicks);
		}
		if (startPitch <= 90) {
			AddControllerPitchInput(-startPitch / ((APlayerController*)Controller)->InputPitchScale / numTicks);
		}
		else {
			AddControllerPitchInput((360.f - startPitch) / ((APlayerController*)Controller)->InputPitchScale / numTicks);
		}
		
		//Lower the camera
		CameraBoom->AddRelativeLocation(FVector(0.f, 0.f, -50.f / numTicks));

		if (CameraBoom->TargetArmLength <= InFlightCameraTrailingDistance) {
			bUseControllerRotationPitch = true;
			bUseControllerRotationYaw = true;
			bUseControllerRotationRoll = true;

			GetCharacterMovement()->SetMovementMode(MOVE_Flying);
			walking = false;
			GetWorldTimerManager().ClearTimer(FlightToggleTimerHandle);

			togglingFlight = false;
		}
	}

	//Flying to walking
	else {
		//Increase camera distance
		float armLengthDiff = DefaultCameraTrailingDistance - InFlightCameraTrailingDistance;
		CameraBoom->TargetArmLength += armLengthDiff / numTicks;

		//Raise the camera
		CameraBoom->AddRelativeLocation(FVector(0.f, 0.f, 50.f / numTicks));

		if (CameraBoom->TargetArmLength >= DefaultCameraTrailingDistance) {
			bUseControllerRotationPitch = false;
			bUseControllerRotationYaw = false;
			bUseControllerRotationRoll = false;

			GetCharacterMovement()->SetMovementMode(MOVE_Walking);

			//Reset actor pitch
			FRotator currRot = GetActorRotation();
			SetActorRotation(FRotator(0, currRot.Yaw, currRot.Roll));

			walking = true;
			GetWorldTimerManager().ClearTimer(FlightToggleTimerHandle);

			togglingFlight = false;
		}
	}
}
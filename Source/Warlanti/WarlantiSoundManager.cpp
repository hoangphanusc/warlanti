// Fill out your copyright notice in the Description page of Project Settings.

#include "WarlantiSoundManager.h"


// Sets default values
AWarlantiSoundManager::AWarlantiSoundManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWarlantiSoundManager::BeginPlay()
{
	Super::BeginPlay();
	backgroundMusic->Play();
}

// Called every frame
void AWarlantiSoundManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "WarlantiGameMode.h"
#include "WarlantiCharacter.h"
#include "UObject/ConstructorHelpers.h"

AWarlantiGameMode::AWarlantiGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

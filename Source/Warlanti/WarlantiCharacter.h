// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "WarlantiCharacter.generated.h"

UCLASS(config=Game)
class AWarlantiCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Warlanti, meta = (AllowPrivateAccess = "true"))
	float DefaultCameraTrailingDistance = 400.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Warlanti, meta = (AllowPrivateAccess = "true"))
	float InFlightCameraTrailingDistance = 200.f;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AWarlantiCharacter();
	void BeginPlay();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	//Warlanti handlers
	void ToggleFlight();
	void Attack1();
	void Attack2();
	void Jump() override;
	void StopJumping() override;
	void DelegateJump();
	void DelegateStopJumping();
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

private:
	//Warlanti vars
	bool walking = true;

	UPROPERTY(BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool pressedJump = false;

	/* Handle to manage the timer */
	FTimerHandle FlightToggleTimerHandle;

	// Time taken to toggle flight, in seconds
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float FlightToggleDelay = 0.5f;

	const float FlightToggleTickRate = 0.0025f;

	// Variable used to lock movement and camera input while toggling flight
	bool togglingFlight;

	// Variables used to rotate camera while toggling flight
	float startYaw, startPitch, targetYaw;

	UFUNCTION()
	void OnFlightToggle();

	UPROPERTY(EditAnywhere, Category = Warlanti)
	TSubclassOf<class AAttackInstanceActor> Attack2Projectile;

	UPROPERTY(EditAnywhere, Category = Warlanti)
	UParticleSystem* Attack1Effect;

	UPROPERTY(EditAnywhere, Category = Warlanti)
	float Attack1Range = 2000.f;

	UPROPERTY(EditAnywhere, Category = Warlanti)
	float Attack1AOE = 600.f;

	UPROPERTY(EditAnywhere, Category = Warlanti)
	float Attack1Damage = 10.f;

public:
	/** Returns CameraBoom subobject **/
	class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	bool GetPressedJump() const { return pressedJump; }
};

